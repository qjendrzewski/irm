<?php
use PHPUnit\Framework\TestCase;
use App\Controllers\CheckoutController;

/**
 * Tests CheckoutController
 * 
 * @category tests
 *
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class CheckoutControllerTest extends TestCase
{
    
    /**
     * Checkout Register
     * 
     * @var App\Controllers\Checkout
     */
    private $_oCheckout = null;
    
    /**
     * Set Up Per-TestCase
     * 
     * @return void
     */
    public function setUp()
    {
        $this->_oCheckout = new CheckoutController();
    }

    /**
     * Test Case:
     * Is Checkout::setPricing() clearing the sale register?
     * 
     * @return void
     */
    public function testSetPricing1()
    {
        
        $_SESSION['sale'] = [
            'AB',
            'CD',
            'EF',
            'GH',
            'IJ'
        ];
        
        $this->_oCheckout->setPricing();
                
        $this->assertFalse( isset( $_SESSION['sale'] ) );
    }

    /**
     * Test Case:
     * Is Checkout::setPricing() executing without failure if the sale register isn't initialised?
     * 
     * @return void
     */
    public function testSetPricing2()
    {
        $this->_oCheckout->setPricing();
                
        $this->assertFalse( isset( $_SESSION['sale'] ) );
    }
    
    /**
     * Test Case:
     * Is Checkout::scanItem() correctly rejecting invalid item codes?
     * 
     * @return void
     */
    public function testAddToRegister1()
    {
        $this->_oCheckout->setPricing();
        $result = $this->_oCheckout->scanItem(123);
                
        $this->assertFalse($result);
        $this->assertFalse( isset( $_SESSION['sale'] ) );
    }
    
    /**
     * Test Case:
     * Is Checkout::scanItem() correctly not registering a non-existent item?
     * 
     * @return void
     */
    public function testAddToRegister2()
    {
        $this->_oCheckout->setPricing();
        $result = $this->_oCheckout->scanItem('AB');
                
        $this->assertFalse($result);
        $this->assertFalse( isset( $_SESSION['sale'] ) );
    }
    
    /**
     * Test Case:
     * Is Checkout::scanItem() correctly registering an existing item?
     * 
     * @return void
     */
    public function testAddToRegister3()
    {
        $this->_oCheckout->setPricing();
        $this->_oCheckout->scanItem('ZA');
                
        $this->assertTrue( isset( $_SESSION['sale'] ) );
        $this->assertArrayHasKey( 0, $_SESSION['sale'] );
        $this->assertTrue( $_SESSION['sale'][0] === 'ZA' );
    }
    
    /**
     * Test Case:
     * Is Checkout::scanItem() correctly registering any valid item?
     * Uses mocking 
     * 
     * @return void
     * 
     * @skipped
     * For some reason, this test is failing to mock the method
     * I suspect this is due to the incompatibility between my PHP (7.3.2) and PHPUnit (5.7)
     */
    public function testAddToRegister4()
    {
        $this->markTestSkipped( "This test is failing to mock the method - probably due to incompatible PHP and PHPUnit versions.  Will it work for you?");
        
        $oMock = $this->createMock(CheckoutController::class);
        $oMock->method('getItemPrices')->willReturn(
            [
                'AB' => [
                    'unit_price' => 2.00,
                    'volume_quantity' => 4,
                    'volume_price' => 7.00
                ]
            ]
        );
        
        $oMock->setPricing();
        $oMock->scanItem('AB');
                
        $this->assertTrue( isset( $_SESSION['sale'] ) );
        $this->assertArrayHasKey( 0, $_SESSION['sale'] );
        $this->assertTrue( $_SESSION['sale'][0] === 'AB' );
    }
    
    
    /**
     * Test Case:
     * Does Checkout::getTotal() return 0 if no items are set?
     * 
     * @return void
     */
    public function testGetTotal1()
    {
        $this->_oCheckout->setPricing();
        $totalPrice = $this->_oCheckout->getTotal();
        
        $this->assertEquals( 0, $totalPrice );
    }
    
    
    /**
     * Test Case:
     * Is the sum for the following correct?
     * ZA,YB,FC,GD,ZA,YB,ZA,ZA
     * 
     * @return void
     */
    public function testGetTotal2()
    {
        $this->_oCheckout->setPricing();
        $this->_oCheckout->scanItem('ZA');
        $this->_oCheckout->scanItem('YB');
        $this->_oCheckout->scanItem('FC');
        $this->_oCheckout->scanItem('GD');
        $this->_oCheckout->scanItem('ZA');
        $this->_oCheckout->scanItem('YB');
        $this->_oCheckout->scanItem('ZA');
        $this->_oCheckout->scanItem('ZA');
        $totalPrice = $this->_oCheckout->getTotal();
        
        $this->assertEquals( 32.4, $totalPrice );
    }
    
    
    /**
     * Test Case:
     * Is the sum for the following correct?
     * FC,FC,FC,FC,FC,FC,FC
     * 
     * @return void
     */
    public function testGetTotal4()
    {
        $this->_oCheckout->setPricing();
        for ($i = 0; $i < 7; $i++) {
            $this->_oCheckout->scanItem('FC');
        }
        $totalPrice = $this->_oCheckout->getTotal();
        
        $this->assertEquals( 7.25, $totalPrice );
    }
    
    
    /**
     * Test Case:
     * Is the sum for the following correct?
     * ZA,YB,FC,GD
     * 
     * @return void
     */
    public function testGetTotal5()
    {
        $this->_oCheckout->setPricing();        
        $this->_oCheckout->scanItem('ZA');
        $this->_oCheckout->scanItem('YB');
        $this->_oCheckout->scanItem('FC');
        $this->_oCheckout->scanItem('GD');
        $totalPrice = $this->_oCheckout->getTotal();        
        $this->assertEquals( 15.40, $totalPrice );
    }
        
    
    
    
    
}
