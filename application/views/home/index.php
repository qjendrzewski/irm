<?php
/**
 * View display logic for the home page
 * 
 * @url /home
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */

defined('APPLICATION_PATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
                
        <script type="text/javascript">
            
            function clear() {
                $.ajax(
                    {
                        url: 'ajax/clear',
                        type: 'post',
                        dataType: 'json',
                        success: function(data, status, xhr) {
                            if (200 == xhr.status) {
                                $('#lblTotal').html(data.total);
                            }
                        }
                    }
                );
            }

            function add(p_code) {
                $.ajax(
                    {
                        url: 'ajax/scan',
                        method: 'post',
                        data: {code: p_code},
                        dataType: 'json',
                        success: function(data, status, xhr) {
                            if (200 == xhr.status) {
                                $('#lblTotal').html(data.total);
                            }
                        }
                    }
                );
            }
            
        </script>
        
        <style>
            #divWrapper {
                margin-left: 50px;
                margin-right: 50px;
            }
            
            .clear-till {
                margin-top: 50px;
                margin-bottom: 30px;
            }
            
            .till-button {
                border: 1px solid activeborder;
                padding: 30px;
                text-align: center;
                margin-bottom: 30px;
                
                
            }
            
            .till-button-link {
                margin-top: auto;
                margin-bottom: auto;
            }
            
        </style>
        
        
        
        
    </head>
    <body>
        
        <div class="jumbotron">
            <h1>Checkout Till</h1>
            <p class="lead">The only shop where you can buy ZA's, FC's, and other two-letter combinations</p>
            <hr class="my-4">
            <p>Press [Clear Till] to start a new transaction</p>
        </div>
        
        <div id='divWrapper'>
            
            <div id='divContent'>
                
                <ul class="list-unstyled">
                    
                    <div class="row ">
                        
                        <div class='col-md-4 clear-till'>
                            
                            <li class='till-button'>
                                <a class="well " href="javascript: clear();">
                                    <h4>Clear Till</h4>
                                </a>
                            </li>
                            
                        </div>
                        
                        <div class='offset-md-4 col-md-4 clear-till'>
                            
                            <li class='till-button'>
                                <label id='lblTotal'>0.00</label>
                            </li>
                            
                        </div>
                        
                    </div>
                    
                    <div class="row ">
                        
                        <div class="col-md-4">
                            
                            <li class='till-button'>
                                <a class="well mr-2" href="javascript: add('ZA');">
                                    <h4>ZA</h4>
                                </a>
                            </li>
                            
                        </div>
                        
                        <div class="col-md-4">
                            
                            <li class='till-button'>
                                <a class="well mr-2" href="javascript: add('YB');">
                                    <h4>YB</h4>
                                </a>
                            </li>
                            
                        </div>
                        
                        <div class="col-md-4">
                            
                            <li class='till-button'>
                                <a class="well mr-2" href="javascript: add('FC');">
                                    <h4>FC</h4>
                                </a>
                            </li>
                            
                        </div>
                        
                        <div class="col-md-4">
                            
                            <li class='till-button'>
                                <a class="well mr-2" href="javascript: add('GD');">
                                    <h4>GD</h4>
                                </a>
                            </li>
                            
                        </div>
                        
                    </div>
                    
                </ul>
                
            </div>
            
        </div>
            
    </body>
</html>