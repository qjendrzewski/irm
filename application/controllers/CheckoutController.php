<?php
namespace App\Controllers;

defined('APPLICATION_PATH') OR exit('No direct script access allowed');

/**
 * Controller class to manage checkout
 * 
 * @category controllers
 *
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class CheckoutController
{
    
    /**
     * Clears the checkout register for a new sale
     * 
     * @return void
     */
    public function setPricing()
    {
        unset( $_SESSION['sale'] );
    }
    
    
    /**
     * Returns the item prices
     * 
     * This is where the item prices are set
     * Ordinarily, my instict is to use a DB, such as MySQL, instead of a multi-dim array.  Would that be too 
     * complicated?  Not necessarily, however I don't want to over-complicate this test submission with mirations 
     * and DB gateway table-models.
     * 
     * On a side note, how would we achieve the array configuration returned by this function if we were using MySQL?
     * Once we have fetched an associative array, PHP's array_column() would help us to isolate the price-code 
     * key-value and we could either leave this value in place in each row or use PHP's array_walk() to kill 
     * the price-code column in each resultant row.
     * 
     * @return array
     */
    public function getItemPrices()
    {
        $arrPrices = [
            'ZA' => [
                'unit_price' => 2.00,
                'volume_quantity' => 4,
                'volume_price' => 7.00
            ],
            'YB' => [
                'unit_price' => 12.00,
                'volume_quantity' => 0,
                'volume_price' => 0.00
            ],
            'FC' => [
                'unit_price' => 1.25,
                'volume_quantity' => 6,
                'volume_price' => 6.00
            ],
            'GD' => [
                'unit_price' => 0.15,
                'volume_quantity' => 0,
                'volume_price' => 0.00
            ],
        ];
        return $arrPrices;
    }
    
    
    /**
     * Adds an item to the checkout register
     * 
     * @param string $p_itemCode Item Code
     * 
     * @return 
     */
    public function scanItem($p_itemCode)
    {
        
        if ( !is_string($p_itemCode) ) {
            return false;
        }
        /**
         * Validate the supplied code by checking it exists
         */
        $arrPrices = $this->getItemPrices();
        if ( !array_key_exists($p_itemCode, $arrPrices) ) {
            return false;
        }
        
        /**
         * Adding item to sales register
         */
        $_SESSION['sale'][] = $p_itemCode;
        
        return true;
        
    }
    
    
    /**
     * Returns the sale total for the registered items
     * 
     * The code is made deliberately debuggable
     * 
     * Assumptions:
     * 
     * - The price array rows hold a 'volume_quantity'  and 'volume_price' element if there are volume prices
     * 
     * 
     * @return numeric
     */
    public function getTotal()
    {
        if ( !isset( $_SESSION['sale'] ) ) {
            return 0;
        }
        
        $arrTotal = array_count_values( $_SESSION['sale'] );
        
        $arrPrices = $this->getItemPrices();
        
        $arrResult = [];
        
        foreach ( $arrTotal as $item=>$count) {
            
            $volumeCount = 0;
            $singleCount = $arrTotal[$item];
            
            if ( isset( $arrPrices[$item]['volume_quantity'] ) && $arrPrices[$item]['volume_quantity'] > 0 ) {
                
                /**
                 * Calculating how many times the volume-quantity goes into the total number of items 
                 */
                $volumeCount = floor( $arrTotal[$item] / $arrPrices[$item]['volume_quantity'] );
                
                /**
                 * Calculating how many single (non-volume) items there are
                 */
                $singleCount = $arrTotal[$item] % $arrPrices[$item]['volume_quantity'];
                
            }
                
            /**
             * Calculating the resultant price
             */
            $price = $volumeCount * $arrPrices[$item]['volume_price'];
            $price = $price + $singleCount * $arrPrices[$item]['unit_price'];
            $arrResult[$item] = $price;
            
        }
        
        $totalPrice = array_sum( array_values($arrResult) );
        
        return $totalPrice;
    }
    
}