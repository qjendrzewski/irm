<?php
namespace App\Controllers;

/**
 * Homepage controller
 * 
 * @category controllers
 *
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class HomeController 
{
    
    /**
     * Default action method
     * 
     * @return void
     */
    public function index()
    {
        $arrView = [
            
        ];
        include(VIEW_PATH . 'home/index.php');
    }
    
}
