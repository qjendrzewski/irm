<?php
namespace App\Controllers;

defined('APPLICATION_PATH') OR exit('No direct script access allowed');

use App\Controllers\CheckoutController;

/**
 * Controller class to manage ajax requests
 * 
 * @category controllers
 *
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
class AjaxController {
    
    
    const HTTP_INTERNAL_ERROR = 500;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_OK = 200;
    
    
    /**
     * @var App\Controllers\CheckoutController
     */
    private $_oCheckout = null;
    
    /**
     * Class constructor
     * 
     * @return void
     */
    public function __construct() 
    {
        $this->_oCheckout = new CheckoutController();
    }
    
    /**
     * Scans in an item
     * 
     * @url ajax/scan
     * 
     * @params string $_POST 'code' Item code
     * 
     * @return json
     */
    public function scan()
    {
        
        if ( !isset( $_POST['code'] ) ) {
            
            $arrResponse = [
                'result' => 0,
                'reason' => 'Code not supplied',
            ];
            http_response_code(self::HTTP_INTERNAL_ERROR);
            echo json_encode($arrResponse);
            exit;
            
        }
        
        $success = $this->_oCheckout->scanItem( $_POST['code'] );
        
        if ($success) {
            
            $total = $this->_oCheckout->getTotal();
            
            $arrResponse = [
                'result' => 1,
                'total' => $total,
            ];
            http_response_code(self::HTTP_OK);
                
        } else {
            
            $arrResponse = [
                'result' => 0,
                'reason' => 'Code failed',
            ];
            http_response_code(self::HTTP_BAD_REQUEST);
            
        }
        
        echo json_encode($arrResponse);
        
    }
    
    /**
     * Clears the till
     * 
     * @url /ajax/clear
     * 
     * @return json
     */
    public function clear()
    {
        $this->_oCheckout->setPricing();
        $total = $this->_oCheckout->getTotal();
        $arrResponse = [
            'result' => 1,
            'total' => $total,
        ];
        http_response_code(self::HTTP_OK);
        
        echo json_encode($arrResponse);
    }
    
}
