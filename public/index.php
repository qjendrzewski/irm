<?php
/**
 * Main execution page for the IRM Tech Test
 * 
 * @category index
 * 
 * @author Quentin Jendrzewski <qljsystems@hotmail.co.uk>
 */
session_start();

error_reporting(E_ALL);

define('APPLICATION_PATH', dirname(__FILE__) . '/../application' . DIRECTORY_SEPARATOR);
define('MODEL_PATH', APPLICATION_PATH . 'models' . DIRECTORY_SEPARATOR);
define('CONTROLLER_PATH', APPLICATION_PATH . 'controllers' . DIRECTORY_SEPARATOR);
define('VIEW_PATH', APPLICATION_PATH . 'views' . DIRECTORY_SEPARATOR);

if ( !defined('ENVIRONMENT') ) {
    define('ENVIRONMENT', 'development');
}

/**
 * Globally required for unit-tests as well as system
 */
require_once(CONTROLLER_PATH . 'CheckoutController.php');


/**
 * Front Controller
 */

$url = $_SERVER['REQUEST_URI'];
$urlPath = parse_url($url, PHP_URL_PATH);
$urlQuery = parse_url($url, PHP_URL_QUERY);

if ( $urlPath ) {
    
    /**
     * Separating preceding slash
     */
    $arrPath = explode('/', $urlPath);
    
    $controller = ucfirst( $arrPath[1] ) . "Controller";
    $method = false;
    if ( isset( $arrPath[2] ) ) {
        $method = $arrPath[2];
    }
    
    if ( file_exists(CONTROLLER_PATH . $controller . ".php") ) {
        require_once(CONTROLLER_PATH . $controller . ".php");
    }
        
    $controller = "App\\Controllers\\{$controller}";

    if ( class_exists($controller) ) {
        $oController = new $controller();
        if ( empty($method) ) {
            $oController->index();
        } else {
            $oController->$method();
        }
    } else {
        exit("This route doesn't exist");
    }
    
}